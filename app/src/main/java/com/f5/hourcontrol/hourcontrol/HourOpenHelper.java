package com.f5.hourcontrol.hourcontrol;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Eduardo on 24/01/15.
 */
public class HourOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "horarios.db";
    private static final int DATABASE_VERSION = 1;

    public HourOpenHelper(Context c){
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE horarios "+
                "(id INTEGER PRIMARY KEY  AUTOINCREMENT,"+
                "dataEntrada	VARCHAR(30)	NOT NULL,"+
                "entrada VARCHAR(30) NOT NULL,"+
                "dataSaida	VARCHAR(30)	,"+
                "saida VARCHAR(30) ,"+
                "saldo DOUBLE );";

        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
