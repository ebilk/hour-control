package com.f5.hourcontrol.hourcontrol;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import at.markushi.ui.CircleButton;


public class MainActivity extends Activity {
    private CircleButton btnEntrada, btnSaida;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnEntrada = (CircleButton) findViewById(R.id.btnEntrada);
        btnSaida = (CircleButton) findViewById(R.id.btnSaida);

        refreshButtons();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_main, menu);
//        return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void doEntrar (View v){
        salvaEntrada();
    }

    public void doSair(View v){
        salvaSaida();
    }

    private boolean faltaEntrar(){
        HourOpenHelper hDb = new HourOpenHelper(this);
        SQLiteDatabase db = hDb.getReadableDatabase();
        String query = "SELECT saida FROM horarios ORDER BY id DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query, null);
        String s = "";

        if (cursor.moveToFirst()){
//            cursor.moveToNext();
            s = cursor.getString(0);
        }
        db.close();
        return s != null;

    }
    private void refreshButtons(){
        if (faltaEntrar()){
            btnEntrada.setVisibility(View.VISIBLE);
            btnSaida.setVisibility(View.INVISIBLE);
        }else{
            btnEntrada.setVisibility(View.INVISIBLE);
            btnSaida.setVisibility(View.VISIBLE);
        }
    }

    private void salvaEntrada(){
        //Insert no DB
        String agr = agora("HH:mm:ss");
        String hj = agora("dd-MM-yyyy");
        //Cria uma instancia de um OpenHelper
        HourOpenHelper hourDb = new HourOpenHelper(this);
        //conecta com o db para escrever
        SQLiteDatabase db = hourDb.getWritableDatabase();
        String query = "INSERT INTO horarios (dataEntrada, entrada) VALUES("+"'" + hj +"', " +"'"+agr+"');";
        //executa a query
        db.execSQL(query);
        //NUNCA esquecer de fechar o banco assim que terminar a query
        db.close();
        Toast.makeText(this,"Registro em " + hj + " as " + agr, Toast.LENGTH_LONG).show();
        refreshButtons();
    }
    private void salvaSaida(){
        //consulta o ultimo saldo no DB e da update na saida de hoje
        double saldo =0;
        int id = 0;
        String entrada, saida = agora("HH:mm:ss");
        //Cria uma instancia de um OpenHelper
        HourOpenHelper hourDb = new HourOpenHelper(this);
        //conecta com o db para escrever
        SQLiteDatabase db = hourDb.getWritableDatabase();
        String querySel = "SELECT id, entrada, saldo FROM horarios ORDER BY id DESC LIMIT 2";
        //executa a query
        Cursor cursor = db.rawQuery(querySel, null);
        if (cursor.moveToFirst()){
//            cursor.moveToNext();
            id = cursor.getInt(0);
            entrada = cursor.getString(1);
            cursor.moveToLast();
            saldo = cursor.getDouble(2);

            System.out.println("id: " + id + ", entrada: " + entrada + ", saida: " + saida + "saldo.");

//          if (calculaSaldoDia(entrada,"08:00:00")<Math.abs(0.25)){
//                entrada = "08:00:00";
//            }
//            if (calculaSaldoDia(saida,"17:00:00")<Math.abs(0.25)){
//                saida = "17:00:00";
//            }
//            System.out.println("id: " + id + ", entrada: " + entrada + ", saida: " + saida + "saldo: " + saldo);

            double saldoDia = calculaSaldoDia(entrada, saida) - 9.00;

            if (saldoDia>0){
                Toast.makeText(this, "Isso ai!! Vc fez " + String.format("%.2f", saldoDia) + " horas hoje.", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "Putz!! Vc vai descontar " + String.format("%.2f",saldoDia) + " horas hoje.", Toast.LENGTH_LONG).show();
            }
            double saldoTotal = saldo + saldoDia;

            String queryUp = "UPDATE horarios SET saida = " + "'" + saida + "'" + ", " +
                    "dataSaida =" + "'" + agora("dd-MM-yyyy") + "'" + ", saldo = " + saldoTotal + " WHERE id =" +
                    " " + id;
            db.execSQL(queryUp);
        }


        //NUNCA esquecer de fechar o banco assim que terminar a query
        db.close();
        refreshButtons();
    }
    private double calculaSaldoDia(String entrada, String saida){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date e = null;
        Date s = null;
        double result = 3;
        try{
            e = format.parse(entrada);
            s = format.parse(saida);

            double diff = s.getTime() - e.getTime();
            System.out.println("diff: " + diff);
            double diffMin = diff/(60*1000) % 60;
            double diffHour = diff / (60*60*1000) % 60;
            System.out.println("diffMin: " + diffMin);
            System.out.println("diffHour: " + diffHour);

            System.out.println("Retorno: " + diffHour + (diffMin/60));
            result = diffHour + (diffMin/60);

        } catch (ParseException e1) {
            e1.printStackTrace();

        }
        return result;
    }


    private String agora( String format){
        // rotina para formatar a data/hora de hoje

        //instancia um calendario
        Calendar c = Calendar.getInstance();
        //seta o formato da data
        SimpleDateFormat df = new SimpleDateFormat(format);// para hora HH:mm:ss | para data "dd-MM-yyyy"
        //retorna o formato
        return df.format(c.getTime());
    }

}
